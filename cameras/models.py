from django.db import models
from base.mixins import TrackedModelMixin
from site_tracker.models import TrackedSite
from .settings import camera_settings


class Camera(TrackedModelMixin, models.Model):
    name = models.CharField(max_length=50)
    host = models.GenericIPAddressField()
    active = models.BooleanField()
    host_tracker = models.OneToOneField(TrackedSite, blank=True, null=True)

    # Track changes to instance
    untracked = ('active',)
    updated = False

    @property
    def url(self):
        return u'{scheme}{hostname}'.format(scheme=camera_settings.URL_SCHEME,
                                            hostname=self.host)

    @property
    def description(self):
        return u'{name} camera'.format(name=self.name)

    @property
    def motion_message(self):
        return u'Motion detected by {description}!'.format(
            description=self.description
        )

    def save(self, *args, **kwargs):
        # Mark new or updated records as updated
        if not self.pk or self.has_changed:
            # Track the new camera's hostname and receive
            # notifications when it goes down
            self.host_tracker, _ = TrackedSite.objects.update_or_create(
                defaults={'name': self.description},
                url=self.url
            )
            self.updated = True
        super(Camera, self).save(*args, **kwargs)

    def __repr__(self):
        return u'<Camera Name:{name} Host:{host}>'.format(name=self.name,
                                                          host=self.host)

    def __unicode__(self):
        return self.name
