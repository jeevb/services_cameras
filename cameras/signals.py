from django.dispatch import receiver
from django.db.models.signals import post_delete
from .models import Camera

@receiver(post_delete, sender=Camera)
def post_delete_tracked_site(sender, instance, *args, **kwargs):
    if instance.host_tracker:
        instance.host_tracker.delete()
