from django.apps import AppConfig

class CamerasConfig(AppConfig):
    name = 'cameras'

    def ready(self):
        from .signals import post_delete_tracked_site
