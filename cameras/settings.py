from django.conf import settings
from django.dispatch import receiver
from django.test.signals import setting_changed
from base.settings import BaseAppSettings

USER_SETTINGS = getattr(settings, 'CAMERA_SETTINGS', None)

DEFAULTS = {
    # DDWRT settings
    'DDWRT_USER': 'root',
    'DDWRT_HOST': '10.0.0.1',
    'DDWRT_PORT': '22',
    'STATIC_LEASE_PATTERN': '_camera',

    'URL_SCHEME': 'http://',
}


class CameraSettings(BaseAppSettings):
    pass


camera_settings = CameraSettings(USER_SETTINGS, DEFAULTS)


@receiver(setting_changed)
def reload_camera_settings(*args, **kwargs):
    global camera_settings
    setting, value = kwargs['setting'], kwargs['value']
    if setting == 'CAMERA_SETTINGS':
        camera_settings = CameraSettings(value, DEFAULTS)
