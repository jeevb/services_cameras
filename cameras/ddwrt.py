import re
import subprocess

from .settings import camera_settings


class DDWRTInterface(object):
    @property
    def static_leases(self):
        proc = subprocess.Popen(
            [
                'ssh',
                '-t',
                '-p', camera_settings.DDWRT_PORT,
                '{user}@{host}'.format(user=camera_settings.DDWRT_USER,
                                       host=camera_settings.DDWRT_HOST),
                'nvram show | grep static_leases'
            ],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )
        output, _ = proc.communicate()

        record_pattern = re.compile(
            r'(?P<MAC_address>([A-Za-z0-9]{2}:){5}[A-Za-z0-9]{2})\='
            r'(?P<name>.+?)\='
            r'(?P<host>(\d{1,3}\.){3}\d{1,3})\='
        )

        name_pattern = camera_settings.STATIC_LEASE_PATTERN
        name_pattern_compiled = re.compile(re.escape(name_pattern))

        for match in record_pattern.finditer(output):
            name = match.group('name')
            if (
                name_pattern is None or
                name_pattern_compiled.search(name)
            ):
                yield {
                    'name': name.replace(name_pattern, ''),
                    'host': match.group('host'),
                }
