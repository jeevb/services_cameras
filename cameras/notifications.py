from rest_framework.views import APIView
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from base.utils import get_client_ip
from .tasks import send_push_notification


class MotionNotification(APIView):
    renderer_classes = (JSONRenderer,)

    def get(self, request):
        client_ip = get_client_ip(request)
        send_push_notification.delay(client_ip)
        return Response()
