import logging

from django.db import transaction
from .models import Camera
from .serializers import CameraSerializer
from .ddwrt import DDWRTInterface

logger = logging.getLogger(__name__)


class Controller(object):
    @property
    def _has_updates(self):
        return (
            any(cam.updated for cam in self._active_queue) or
            bool(self._delete_queue)
        )

    def _update_cameras(self):
        # Temporarily set all existing camera records to be inactive
        Camera.objects.update(active=False)

        # Extract static leases from router
        static_leases = list(DDWRTInterface().static_leases)
        updated = CameraSerializer(data=static_leases, many=True)
        updated.is_valid(raise_exception=True)

        # Add active cameras
        for static_lease in updated.validated_data:
            Camera.objects.update_or_create(defaults=static_lease,
                                            name=static_lease.get('name'))

        # Purge inactive cameras
        delete_queue = Camera.objects.filter(active=False)
        for cam in delete_queue:
            logger.info('Deleting inactive camera: %s' % repr(cam))

        delete_queue.delete()

    def update(self):
        try:
            logger.info('Starting camera update...')
            with transaction.atomic():
                self._update_cameras()
        except Exception, e:
            logger.exception(e)
        else:
            logger.info('Camera update successfully completed.')
