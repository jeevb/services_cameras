from django.contrib import admin
from base.mixins import ReadOnlyModelAdminMixin
from .models import Camera


@admin.register(Camera)
class CameraAdmin(ReadOnlyModelAdminMixin, admin.ModelAdmin):
    list_display = ('name', 'host',)
    search_fields = ('name', 'host',)
