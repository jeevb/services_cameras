# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import base.mixins


class Migration(migrations.Migration):

    dependencies = [
        ('site_tracker', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Camera',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('host', models.GenericIPAddressField()),
                ('active', models.BooleanField()),
                ('host_tracker', models.OneToOneField(null=True, blank=True, to='site_tracker.TrackedSite')),
            ],
            bases=(base.mixins.TrackedModelMixin, models.Model),
        ),
    ]
