from celery import shared_task
from pushover.pushover import PushoverNotifier
from .controller import Controller
from .models import Camera

@shared_task
def heartbeat():
    Controller().update()

@shared_task
def send_push_notification(client_ip):
    try:
        camera = Camera.objects.get(host=client_ip)
    except Camera.DoesNotExist:
        pass
    else:
        PushoverNotifier().push(user=None, message=camera.motion_message)
