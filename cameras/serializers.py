from rest_framework import serializers
from .models import Camera


class CameraSerializer(serializers.ModelSerializer):
    active = serializers.BooleanField(default=True)

    class Meta:
        model = Camera
        exclude = ('id',)
